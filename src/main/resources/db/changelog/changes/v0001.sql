create table "user" (
  id integer not null,
  name varchar(50) not null,
  email varchar(50) not null,
  primary key (id)
);

create table "message" (
  id integer not null,
  text varchar(250) not null,
  date date not null,
  primary key (id)
);