package id.ac.ui.cs.advprog.midterm.entity;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Date;

@Entity
@Table(name="message", schema = "public")
public class Message {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "hibernate_sequence")
    @SequenceGenerator(name = "hibernate_sequence", initialValue = 1)
    @Column(name = "id")
    private long id;

    @NotBlank(message = "Please leave a message")
    @Column(name = "text")
    private String text;

    @Column(name = "date")
    private Date date = new Date();

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
