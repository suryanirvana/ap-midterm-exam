package id.ac.ui.cs.advprog.midterm.controller;

import id.ac.ui.cs.advprog.midterm.entity.Message;
import id.ac.ui.cs.advprog.midterm.entity.User;
import id.ac.ui.cs.advprog.midterm.repository.MessageRepository;
import id.ac.ui.cs.advprog.midterm.repository.UserRepository;
import id.ac.ui.cs.advprog.midterm.service.MessageService;
import id.ac.ui.cs.advprog.midterm.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
public class MainController {

    private static final String USERS = "users";
    private static final String MESSAGES = "messages";
    private static final String INDEX = "index";
    private static final String REDIRECT = "redirect:/";

    @Autowired
    UserRepository userRepository;

    @Autowired
    UserService userService;

    @Autowired
    MessageRepository messageRepository;

    @Autowired
    MessageService messageService;

    @GetMapping("/")
    public String homepage(Model model) {
        model.addAttribute(USERS, userService.getAllUsers());
        model.addAttribute(MESSAGES, messageService.getAllMessages());
        return INDEX;
    }

    @GetMapping("/signup")
    public String showSignUpForm(User user) {
        return "add-user";
    }

    @PostMapping("/adduser")
    public String addUser(@Valid User user, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "add-user";
        }
        userRepository.save(user);
        return REDIRECT;
    }

    @GetMapping("/edit/{id}")
    public String showUpdateForm(@PathVariable("id") long id, Model model) {
        if(!userRepository.existsById(id)) {
            throw new IllegalArgumentException("Invalid user Id:" + id);
        }
        User user = userRepository.findById(id);
        model.addAttribute("user", user);
        return "update-user";
    }

    @PostMapping("/update/{id}")
    public String updateUser(@PathVariable("id") long id, @Valid User user,
                             BindingResult result, Model model) {
        if (result.hasErrors()) {
            user.setId(id);
            return "update-user";
        }
        userService.updateUser(user);
        return REDIRECT;
    }

    @GetMapping("/delete/{id}")
    public String deleteUser(@PathVariable("id") long id, Model model) {
        userService.removeUserById(id);
        return REDIRECT;
    }

    @GetMapping("/addmsg")
    public String showMessageForm(Message message) {
        return "add-message";
    }

    @PostMapping("/addmessage")
    public String addMessage(@Valid Message message, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "add-message";
        }
        messageService.addMessage(message);
        return REDIRECT;
    }

    @GetMapping("/deletemessage/{id}")
    public String deleteMessage(@PathVariable("id") long id, Model model) {
        messageService.removeMessageById(id);
        return REDIRECT;
    }
}
