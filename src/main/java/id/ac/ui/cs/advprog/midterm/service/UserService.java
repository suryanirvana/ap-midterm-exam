package id.ac.ui.cs.advprog.midterm.service;

import id.ac.ui.cs.advprog.midterm.entity.User;
import java.util.List;

public interface UserService {

    List<User> getAllUsers();

    void addUser(User user);

    void removeUserById(long id);

    User getUserById(long id);

    void updateUser(User user);
}
