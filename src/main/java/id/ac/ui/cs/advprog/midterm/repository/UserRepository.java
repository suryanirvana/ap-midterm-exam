package id.ac.ui.cs.advprog.midterm.repository;

import id.ac.ui.cs.advprog.midterm.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {
    List<User> findByName(String name);
    List<User> findAllByOrderByIdAsc();
    User findById(long id);
}
