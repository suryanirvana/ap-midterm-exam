package id.ac.ui.cs.advprog.midterm.controller;
import id.ac.ui.cs.advprog.midterm.entity.Message;
import id.ac.ui.cs.advprog.midterm.entity.User;
import id.ac.ui.cs.advprog.midterm.service.MessageService;
import id.ac.ui.cs.advprog.midterm.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import java.util.Calendar;
import java.util.Date;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class MainControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UserService userService;

    @Autowired
    private MessageService messageService;

    @Mock
    private BindingResult bindingResult;

    @Mock
    private Model model;

    private User user;
    private Message message;
    private String idString;
    private Date date = new Date(0);


    @BeforeEach
    void setUpUser() {
        user = new User();
        user.setId(1);
        user.setName("Surya");
        user.setEmail("abc@xyz.com");
        userService.addUser(user);
        message = new Message();
        message.setId(1);
        message.setText("This is a message");
        message.setDate(date);
        messageService.addMessage(message);
        idString = Long.toString(user.getId());
    }

    @Test
    void checkIfShowHomepageTemplate() throws Exception{
        mockMvc.perform(get("/"))
                .andExpect(status().isOk())
                .andExpect(view().name("index"));
    }

    @Test
    void checkIfShowSignUpTemplate() throws Exception{
        mockMvc.perform(get("/signup"))
                .andExpect(status().isOk())
                .andExpect(view().name("add-user"));
    }

    @Test
    void checkIfAddUserValidThenShowTemplate() throws Exception {
        mockMvc.perform(post("/adduser")
                .flashAttr("user", user)
                .flashAttr("result", bindingResult)
                .flashAttr("model", model))
                .andExpect(view().name("redirect:/"));
    }

    @Test
    void checkIfAddUserNotValidThenStay() throws Exception {
        user.setEmail(null);
        mockMvc.perform(post("/adduser")
                .flashAttr("user", user)
                .flashAttr("result", bindingResult)
                .flashAttr("model", model))
                .andExpect(view().name("add-user"));
    }

    @Test
    void checkIfValidThenShowShowUpdateFormTemplate() throws Exception{
        mockMvc.perform(get("/edit/" + Long.toString(userService.getAllUsers().get(0).getId())))
                .andExpect(model().attributeExists("user"))
                .andExpect(view().name("update-user"));
    }

    @Test
    void checkIfShowUpdateFormTemplateButIdNotFoundThenThrowException() throws Exception{
        try {
            mockMvc.perform(get("/edit/0"))
                    .andExpect(view().name("update-user"));
        } catch (Exception e) {
            assertEquals("Request processing failed; nested exception is java.lang.IllegalArgumentException: Invalid user Id:0", e.getMessage());
        }
    }

    @Test
    void checkIfUpdateUserValidThenShowTemplate() throws Exception {
        mockMvc.perform(post("/update/" + Long.toString(userService.getAllUsers().get(0).getId()))
                .flashAttr("user", user)
                .flashAttr("result", bindingResult)
                .flashAttr("model", model))
                .andExpect(view().name("redirect:/"));
    }

    @Test
    void checkIfUpdateUserNotValidThenStay() throws Exception {
        user.setEmail(null);
        mockMvc.perform(post("/update/" +  idString)
                .flashAttr("user", user)
                .flashAttr("result", bindingResult)
                .flashAttr("model", model))
                .andExpect(status().isOk())
                .andExpect(view().name("update-user"));
    }

    @Test
    void checkIfValidThenShowDeleteUserTemplate() throws Exception{
        mockMvc.perform(get("/delete/" + idString))
                .andExpect(view().name("redirect:/"));
    }

    @Test
    void checkIfShowDeleteUserTemplateButIdNotFoundThenThrowException() {
        try {
            mockMvc.perform(get("/delete/0"))
                    .andExpect(view().name("redirect:/"));
        } catch (Exception e) {
            assertEquals("Request processing failed; nested exception is org.springframework.dao.EmptyResultDataAccessException: No class id.ac.ui.cs.advprog.midterm.entity.User entity with id 0 exists!", e.getMessage());
        }
    }

    @Test
    void checkIfShowMessageFormTemplate() throws Exception{
        mockMvc.perform(get("/addmsg"))
                .andExpect(status().isOk())
                .andExpect(view().name("add-message"));
    }

    @Test
    void checkIfAddMessageValidThenShowTemplate() throws Exception {
        mockMvc.perform(post("/addmessage")
                .flashAttr("message", message)
                .flashAttr("result", bindingResult)
                .flashAttr("model", model))
                .andExpect(view().name("redirect:/"));
    }

    @Test
    void checkIfAddMessageNotValidThenStay() throws Exception {
        message.setText(null);
        mockMvc.perform(post("/addmessage")
                .flashAttr("message", message)
                .flashAttr("result", bindingResult)
                .flashAttr("model", model))
                .andExpect(view().name("add-message"));
    }

    @Test
    void checkIfValidThenShowDeleteMessageTemplate() throws Exception{
        mockMvc.perform(get("/deletemessage/2"))
                .andExpect(view().name("redirect:/"));
    }

    @Test
    void checkIfShowDeleteMessageTemplateButIdNotFoundThenThrowException() {
        try {
            mockMvc.perform(get("/deletemessage/0"))
                    .andExpect(view().name("redirect:/"));
        } catch (Exception e) {
            assertEquals("Request processing failed; nested exception is org.springframework.dao.EmptyResultDataAccessException: No class id.ac.ui.cs.advprog.midterm.entity.Message entity with id 0 exists!", e.getMessage());
        }
    }
}
