package id.ac.ui.cs.advprog.midterm.service;

import id.ac.ui.cs.advprog.midterm.entity.User;
import id.ac.ui.cs.advprog.midterm.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import javax.persistence.EntityNotFoundException;
import static org.junit.jupiter.api.Assertions.*;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class UserServiceImplTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    private User user;

    @BeforeEach
    void setUpUser() {
        user = new User();
        user.setId(1);
        user.setName("Surya");
        user.setEmail("abc@xyz.com");
        userService.addUser(user);
    }

    @Test
    void checkIfSuccessfullyAddUser() {
        assertEquals(1, userRepository.count());
    }

    @Test
    void checkIfGetUserByIdNotFoundThenThrowException() {
        try {
            userService.getUserById(0);
        } catch (EntityNotFoundException e) {
            assertEquals(null, e.getMessage());
        }
    }

    @Test
    void checkIfGetUserByIdFoundThenReturn() {
        User testUser = new User();
        testUser.setId(2);
        testUser.setName("Sarah");
        testUser.setEmail("sarah@abc.com");
        userService.addUser(testUser);
        try {
            assertEquals(testUser.getId(), userService.getUserById(2).getId());
        } catch (EntityNotFoundException e) {
            assertNull(e.getMessage());
        }
    }

    @Test
    void checkIfUserNotFoundThenThrowException() {
        user.setId(0);
        try {
            userService.updateUser(user);
        } catch (EntityNotFoundException e) {
            assertNull(e.getMessage());
        }
    }
}
