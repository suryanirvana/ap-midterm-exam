package id.ac.ui.cs.advprog.midterm.service;

import id.ac.ui.cs.advprog.midterm.entity.Message;
import id.ac.ui.cs.advprog.midterm.repository.MessageRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.persistence.EntityNotFoundException;
import java.util.Calendar;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class MessageServiceImplTest {

    @Autowired
    private MessageRepository messageRepository;

    @Autowired
    private MessageService messageService;

    private Message message;
    private Date date = new Date(0);

    @BeforeEach
    void setUpMessage() {
        message = new Message();
        message.setId(1);
        message.setText("This is a message");
        message.setDate(date);
        messageService.addMessage(message);
    }

    @Test
    void checkIfSuccessfullyAddMessage() {
        assertEquals(1, messageRepository.count());
    }

    @Test
    void checkIfSuccessfullyDeleteMessage() {
        Message deleted = new Message();
        deleted.setId(2);
        deleted.setText("Deleted message");
        deleted.setDate(date);
        messageService.addMessage(deleted);
        assertEquals(deleted.getId(),messageService.getMessageById(2).getId());
        messageService.removeMessageById(2);
        assertEquals(1, messageRepository.count());
    }

    @Test
    void checkIfGetMessageByIdNotFoundThenThrowException() {
        try {
            messageService.getMessageById(0);
        } catch (EntityNotFoundException e) {
            assertEquals(null, e.getMessage());
        }
    }

    @Test
    void checkIfGetMessageByIdFoundThenReturn() {
        Message testMessage = new Message();
        testMessage.setId(3);
        testMessage.setText("Hi there");
        testMessage.setDate(date);
        messageService.addMessage(testMessage);
        assertEquals(testMessage.getId(), messageService.getMessageById(3).getId());
    }
}
